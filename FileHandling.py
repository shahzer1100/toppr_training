#Writing to a file

f = open('myfile.txt', 'w')
f.write('this first line\n')
f.write('this second line\n')
f.close()    

# Reading a file
f = open('myfile.txt', 'r')
print f.read()
# print f.readlines() 
f.close()

# Appending the data

f = open('myfile.txt', 'a')
f.write('this is third line\n')
f.close()

# Binary reading and writing

import pickle
f = open('pick.dat', 'wb')
pickle.dump(11, f)
pickle.dump("this is a line", f)
pickle.dump([1, 2, 3, 4], f)
f.close()

f = open('pick.dat', 'rb')
print pickle.load(f)
print pickle.load(f)
print pickle.load(f)
f.close()
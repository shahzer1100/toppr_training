print "Hello World"

random_value = 100

random_string = "Python is awesome"

# DataTypes
# Numbers

x = int(raw_input())
print x * 10

#String
name = "tom"
mychar = 'a'

string = "this is a bad spam" * 3
print string

test = "welcome"
print test[1 : 4]
print test[: 5]
print test[1 : -1]
print len(test)
print max(test)
print min(test)
print "wel" in test
print "abc" in test

test = "welcome to python"
print test.isalnum()
print "Welcome".isalpha()
print "2012".isdigit()
print test.islower()

print test.endswith("thon")
print test.startswith("good")
print test.find("come")
print test.find("become")
print test.rfind("o")
print test.count("o")

test = "string in python"
print test.capitalize()
print test.title()
print test.lower()
print test.upper()
print test.swapcase()

# List
l1 = [1,2, 3, 4]
l2 = ["this is a string", 12]
print l1
print l2

list1 = [2, 3, 4, 1, 32]
print 2 in list1
print 33 not in list1
print len(list1)
print max(list1)
print sum(list1)

list2 = [11, 33, 44, 66, 788, 1]
print list2[0 : 5]
print list2[ : 3]

list3 = [2, 3, 4, 1, 32, 4]
list3.append(19)
print list3.count(4)
list4 = [99, 54]
list3.extend(list4)

print list3.index(4)
list3.pop(2)

print list3

list3.reverse()
print list3

list3.sort()
print list3


#Dictionaries

friends = {
	'tom': '111-222-333', 
	'jerry': '666-33-1111'
}

dict_emp = {}

print friends['tom']

for key in friends:
	print friends[key]

print friends

print friends.keys()
print friends.values()
print friends.pop('tom')
print friends

# Functions and Loops

def sum(start, end):
   result = 0
   for i in range(start, end + 1):
       result += i
   print(result)
 
sum(10, 50)



# Exception Handling

#Basic Exception Handling

try:
	f = open("somefile.txt", "r")
	print f.read()
	f.close()
except IOError:
	print 'file not found'

try:
	num1, num2 = eval(input("Enter two numbers, separated by a comma : "))
	result = num1 / num2
	print "Result is ", result

except ZeroDivisionError:
	print "Division by zero error"

except SyntaxError:
	print "Comma is missing. Enter the numbers separated by comma"

except:
	print "Wrong input"

finally:
	print "This will execute"


# Raise exceptions

def enterage(age):
	if age < 0:
		raise ValueError("Only positive integer allowed")

	if age % 2 == 0:
		print "age is even"
	else:
		print "age is odd"

try:
	num = int(input("Enter your age  "))
	enterage(num)

except ValueError:
	print "Only positive integer allowed"
except:
	print "Something is wrong"


# Using custom exception classes

class NegativeAgeException(RuntimeError):

	def __init__(self, age):
		super().__init__()
		self.age = age

def enterageWithCustomExceptionClass(age):
	if age < 0:
		raise NegativeAgeException("Only positive integer allowed")

	if age % 2 == 0:
		print "age is even"
	else:
		print "age is odd"


try:
	num = int(input("Enter your age  "))
	enterageWithCustomExceptionClass(num)

except ValueError:
	print "Only positive integer allowed"
except:
	print "Something is wrong"
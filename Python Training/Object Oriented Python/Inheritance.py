class Vehicle:
	
	def __init__(self, name, color):
		self.__name = name
		self.__color = color


	def getColor(self):
		return self.__color

	def setColor(self, color):
		self.__color = color


	def getName(self):
		return self.__name

class Car(Vehicle):

	def __init__(self, name, color, model):
#		super(Car, self).__init__(name, color)
		super(self).__init__(name, color)
		self.__model = model

	def getDescription(self):
		return self.getName() + self.__model + " in " + self.__color

c = Car("Ford Mustang", "red", "GT350")
print c.getDescription()
print c.getName()

# Method Overriding
class A():

	def __init__(self):
		self.__x = 1

	def m1(self):
		print "m1 from A"

class B(A):

	def __init__(self):
		self.__y = 1

	def m1(self):
		print "m1 from B"

c = B()
c.m1()
class BankAccount:

	#constructor or initializer
	def __init__(self, name, money):
		self.__name = name
		self.__balance = money

	def deposit(self, money):
		self.__balance += money

	def withdraw(self, money):
		if(self.__balance > money):
			self.__balance -= money
			return money
		else:
			return "Insufficient funds"

	def checkbalance(self):
		return self.__balance

b1 = BankAccount('Shahzer', 100000)
print b1.withdraw(5000)
b1.deposit(1000)
print b1.checkbalance()
b1.withdraw(1800)
print b1.checkbalance()
from django.db import models
#from django.contrib.auth import models
from django.utils import timezone
from .search import BlogPostIndex

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def indexing(self):
        print (self.title)
        obj = BlogPostIndex(
            meta={'id': self.id},
            author=self.author.username,
            published_date=self.published_date,
            title=self.title,
            text=self.text,
            created_date = self.created_date
        )
        obj.save()
        return obj.to_dict(include_meta=True)

class BlogPost(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='blogpost')
    posted_date = models.DateField(default=timezone.now)
    title = models.CharField(max_length=200)
    text = models.TextField(max_length=1000)

    # Method for indexing the model
    def indexing(self):
        obj = BlogPostIndex(
            meta={'id': self.id},
            author=self.author.username,
            posted_date=self.posted_date,
            title=self.title,
            text=self.text
        )
        print (obj.author)
        obj.save()
        return obj.to_dict(include_meta=True)

class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

class Musician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    instrument = models.CharField(max_length=100)

class Album(models.Model):
    artist = models.ForeignKey(Musician)
    name = models.CharField(max_length=100)
    release_date = models.DateField()
    num_stars = models.IntegerField()

class Blog(models.Model):
    name = models.CharField(max_length=100)
    tagline = models.TextField()
    
    def __unicode__(self):
        return self.name

class Author(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    
    def __unicode__(self):
        return self.name

class Entry(models.Model):
    blog = models.ForeignKey(Blog)
    headline = models.CharField(max_length=255)
    body_text = models.TextField()
    pub_date = models.DateTimeField()
    mod_date = models.DateTimeField()
    authors = models.ManyToManyField(Author)
    n_comments = models.IntegerField()
    n_pingbacks = models.IntegerField()
    rating = models.IntegerField()
    
    def __unicode__(self):
        return self.headline
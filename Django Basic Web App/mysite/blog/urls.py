from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.check_login_request, name='check_login_request'),
	url(r'^login$', views.login_user, name='login_user'),
    url(r'^view$', views.post_list, name='post_list'),
    url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^post/(?P<pk>\d+)/edit/$', views.post_edit, name='post_edit'),
    url(r'^logout$', views.logout, name='logout'),

    # url(r'^post/$', views.post_detail, name='post_detail'),
    # url(r'^shahzer/123/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
]
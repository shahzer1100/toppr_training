from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post
from .forms import PostForm
from .search import current_indexing
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login


# Create your views here.

def check_login_request(request):
	if('id' in request.session.keys()):
		try:
			print (request.session.__getitem__('id'))
			user = User.objects.get(pk = request.session.__getitem__('id'))
			print ("You are already logged in")
			return redirect('post_list')
		except:
			print ("Please login")
			return render(request, 'blog/login.html')
	else:
		return render(request, 'blog/login.html')

def login_user(request):
	print ("hello")
	print (request.POST['uname'])
	user = authenticate(username=request.POST['uname'], password=request.POST['psw'])
	if user is not None:
		if user.is_active:
			login(request, user)
			print ("You provided a correct username and password!")
			request.session.__setitem__('id', user.id)
			return redirect('post_list')
		else:
		    print ("Your account has been disabled!")
	else:
		print ("Your username and password were incorrect.")
		return render(request, 'blog/login.html')

def logout(request):
	request.session.clear()
	return render(request, 'blog/login.html')
	
def post_list(request):
	if(request.session.__contains__('id')):
		print (request.session.keys())
		posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
		request.session.__setitem__('key', 'value')
		request.session.__setitem__('shahzer', 'husain')
		return render(request, 'blog/post_list.html', {'posts' : posts, 'flag' : True})	
	else:
		return render(request, 'blog/login.html')

def post_detail(request, pk):
	if(request.session.__contains__('id')):
		post = get_object_or_404(Post, pk=pk)
		return render(request, 'blog/post_detail.html', {'post': post, 'flag' : True})
	else:
		return render(request, 'blog/login.html')

def post_new(request):
	if(request.session.__contains__('id')):
		if request.method == "POST":
			form = PostForm(request.POST)
			if form.is_valid():
				post = form.save(commit=False)
				post.author = request.user
				post.published_date = timezone.now()
				post.save()
				#current_indexing(post)
				return redirect('post_detail', pk=post.pk)
		else:
				form = PostForm()
		return render(request, 'blog/post_edit.html', {'form': form, 'flag' : True})
	else:
		return render(request, 'blog/login.html')

def post_edit(request, pk):
	if(request.session.__contains__('id')):
		post = get_object_or_404(Post, pk=pk)
		if request.method == "POST":
			form = PostForm(request.POST, instance=post)
			if form.is_valid():
				post = form.save(commit=False)
				post.author = request.user
				post.published_date = timezone.now()
				post.save()
				return redirect('post_detail', pk=post.pk)
		else:
			form = PostForm(instance=post)
		return render(request, 'blog/post_edit.html', {'form': form, 'flag': True})
	else:
		return render(request, 'blog/login.html')